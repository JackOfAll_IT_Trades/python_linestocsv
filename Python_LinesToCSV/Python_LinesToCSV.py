#
# Python_LinesToCSV:
#
# This script converts a line-by-line file into a CSV file.
#
# This code is copyright (C) April, 2019  JackOfAll_IT_Trades
# (JackOfAll.IT.Trades@gmail.com).
#

import os
import os.path

import sys

import argparse


parser = argparse.ArgumentParser(description='Converts a line-by-line file into a CSV file.')

parser.add_argument(   '--inputFile', metavar='<path/to/file.txt>', type=argparse.FileType('r'))
parser.add_argument(  '--outputFile', metavar='<path/to/file.csv>', type=argparse.FileType('w'))
parser.add_argument( '--headerLines', metavar='N',                  type=int)

args = parser.parse_args()

if not(args.inputFile) :
	print("")
	print("ERROR:  Please specify input file.")
	print("")
	parser.print_help()
	sys.exit(10)
#
# if not(args.inputFile) : ...

if not(args.headerLines) :
	print("")
	print("ERROR:  Please specify number of header lines.")
	print("")
	parser.print_help()
	sys.exit(20)
#
# if not(args.headerLines) : ...

if not(args.outputFile) :
	outputFile_baseName = os.path.splitext(args.inputFile.name)[0]
	outputFile_fullPath = outputFile_baseName + ".csv"

	args.outputFile = open(outputFile_fullPath, mode="w")
#
# if not(args.outputFile) : ...

inputLines = args.inputFile.readlines()

strippedInput = []
for inputLine in inputLines:
	if inputLine.endswith("\n") :
		strippedInput.append( inputLine[0:-1] )
	else:
		strippedInput.append( inputLine )

# lastHeaderLine = args.headerLines - 1
lastHeaderLine   = args.headerLines

headerStrings = strippedInput[0:lastHeaderLine]

headerOutputLine = ",".join(headerStrings) + "\n"

args.outputFile.write(headerOutputLine)


dataLines = strippedInput[lastHeaderLine:]


print("Beginning output...")

currentLineNum  = 0
currentLineStr  = ""
currentFieldNum = 0

for dataLine in dataLines:
	currentLineNum += 1

	currentFieldNum += 1
	if len(currentLineStr) > 0:
		currentLineStr += ","

	currentLineStr += dataLine

	if currentFieldNum >= lastHeaderLine:
		args.outputFile.write(currentLineStr)
		args.outputFile.write("\n")
		print(currentLineStr)
		currentLineStr  = ""
		currentFieldNum = 0
	#
	# END if currentFieldNum > lastHeaderLine: ...

#
# END for dataLine in dataLines: ...


if currentLineStr != "":
	args.outputFile.write(currentLineStr)
	args.outputFile.write("\n")
	print(currentLineStr)
	currentLineStr  = ""
	currentFieldNum = 0
#
# END if currentLineStr != "": ...

print("All done! :)")

